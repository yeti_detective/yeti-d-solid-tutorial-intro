const defaultContainer = "https://user.databox.me/Public/bins";
const $rdf = require("rdflib");

console.log("IF YOU ARE READING THIS IN CHROME DEVTOOLS THEN I AM REAL CODE THAT RUNS!");

import vocab from "./vocab";

export function publish() {
  // Bin structure
  const bin = {
    url: "",
    title: "",
    body: ""
  };
  bin.title = document.getElementById("edit-title").value;
  bin.body = document.getElementById("edit-body").value;

  const graph = $rdf.graph();
  const thisResource = $rdf.sym('');
  graph.add(thisResource, vocab.dct('title'), $rdf.lit(bin.title));
}
