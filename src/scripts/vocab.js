const dct = term => {
  // full Dublin Core schema can be found at https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
  const termLookup = {
    title: {
      URI: "http://purl.org/dc/terms/title",
      Label: "Title",
      Definition: "A name given to the resource",
      "Type of Term": "Property",
      "Has Range": "http://www.w2.org/2000/01/rdf-schema#Literal",
      "Subproperty of": "http://purl.org/dc/elements/1.1/title"
    }
  };

  return termLookup[term] || null;
};

const sioc = term => {
  // full Social Interconnected Online Communitities spec can be found at http://rdfs.org/sioc/spec/
  const termLookup = {
    content: {
      Property: "sioc:content",
      content: "The content of the Item in plain text format.",
      "OWL Type": "DatatypeProperty",
      Domain: "sioc:Item",
      Range: "rdfs:Literal",
      what:
        "This property is for a plain-text rendering of the content of an Item. Rich content (e.g. HTML, wiki markup, BBCode, etc.) can be described using the Content class from AtomOwl or the content:encoded property from the RSS 1.0 Content Module."
    }
  };
  return termLookup[term] || null;
};

export const vocab = {
  dct,
  sioc
};

module.exports = vocab;
