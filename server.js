const express = require("express");
const path = require("path");

const app = express();
const PORT = process.env.port || 8888;

const returnPath = fileName => {
  return path.join(__dirname, "dist", fileName);
};

// how about some header-logging middleware
const logHeader = (req, res, next) => {
  console.log(JSON.stringify(req.headers));
  next();
};

// wait, that was easy
app.use(logHeader);

app.get("/", (req, res) => {
  res.sendFile(returnPath("index.html"));
});

app.get("/script", (req, res) => {
  res.sendFile(returnPath("bundle.js"));
});

app.listen(PORT, () => {
  console.log(`server listening on port ${PORT}`);
});
